@extends('master')

@section('judul')
    Tambah Cast
@endsection

@section('judul1')
    Tambah Cast
@endsection

@section('content')

            <form action="/cast" method="POST">
                @csrf
                <div class="form-group">
                    <label>Nama Cast</label>
                    <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan nama cast">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Umur Cast</label>
                    <input type="text" class="form-control" name="umur" id="body" placeholder="Masukkan umur">
                    @error('umur')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Bio Cast</label>
                    <textarea name="bio"  class="form-control"></textarea>
                    @error('bio')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Tambah</button>
            </form>

@endsection